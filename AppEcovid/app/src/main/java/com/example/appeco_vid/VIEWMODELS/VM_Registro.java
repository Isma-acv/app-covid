package com.example.appeco_vid.VIEWMODELS;

public class VM_Registro {
    public String mail;
    public String usuario;
    public String nombre;
    public String password;
    public String cuenta;
    public String token;
    public String detalle;
    public String estado;

    public VM_Registro(String usuario, String password,String mail,String nombre,String cuenta,String token){
        this.usuario = usuario;
        this.password = password;
        this.mail = mail;
        this.nombre = nombre;
        this.cuenta = cuenta;
        this.token = token;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


}
