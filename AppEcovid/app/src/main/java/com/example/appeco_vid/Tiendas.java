package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.Clase_Comercios;
import com.example.appeco_vid.VIEWMODELS.VM_Comercios;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Tiendas extends AppCompatActivity {

    // variables que obtengo
    public String giro;
    public TextView tvgiro;
    // AREGLOS QUE DONDE SE VAN A GUADAR LA RESPUESTA DEL JSON
    ArrayList<String> Catalogonombres = new ArrayList<>();
    ArrayList<String> Catalgotelefonos = new ArrayList<>();
    ArrayList<String> Catalogoestados = new ArrayList<>();
    ArrayList<String> Catalogomunicipios = new ArrayList<>();
    ArrayList<String> Catalogocolonia = new ArrayList<>();
    ArrayList<String> Catalogotokent = new ArrayList<>();
    ArrayList<String> CatalogoId = new ArrayList<>();

    //-- VARIABLES DE LOS ELEMENTOS QUE VOY A OCUPAR
    public ListView lvtiendas;
    public TextView tvnombre, tvtelefonos, tvestados,tvmunicioios,tvcolonias;

    //-- ARREGLOS QUE VAN A HEREDAR EL CONTENIDO DE LOS ARRAYLIST
    public String ctelefonos[], cestados[], cmunicipios[], ccolonias[], ctokenes[], cid[];
    public String Token_tineda, id_tienda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tiendas);
        //obtengo el giro de la tienda
        tvgiro = (TextView) findViewById(R.id.textViewtiendas);
        giro = getIntent().getStringExtra("girocomercio");
        tvgiro.setText(giro);

        //-- elemntos de mi textViws
        tvnombre = (TextView) findViewById(R.id.textViewNTienda);
        tvtelefonos = (TextView) findViewById(R.id.textViewTElefono);
        tvestados = (TextView) findViewById(R.id.textViewEStado);
        tvmunicioios = (TextView) findViewById(R.id.textViewMUnicipio);
        tvcolonias = (TextView) findViewById(R.id.textViewCOlonia);
        lvtiendas = (ListView) findViewById(R.id.lvtiendas);
        //--------------------------------------------- Codigo ListView
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.list_item_disenolu,Catalogonombres);
        lvtiendas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvnombre.setText("Tienda: " +lvtiendas.getItemAtPosition(position));
                tvtelefonos.setText("Telefono: "+ ctelefonos[position]);
                tvestados.setText("Estado: "+ cestados[position]);
                tvmunicioios.setText("Municipio: "+ cmunicipios[position]);
                tvcolonias.setText("Colonia: "+ ccolonias[position]);
                Token_tineda = ctokenes[position];
                id_tienda = cid[position];
            }
        });
        //cierre Button para la lista de usuarios

        //------------------------------------------------    Codigo Peticion   --------------------------------------------------
        ServicioPeticion service = Api.getApi(Tiendas.this).create(ServicioPeticion.class);
        Call<VM_Comercios> iniciarCall = service.get_Comercios(tvgiro.getText().toString());
        iniciarCall.enqueue(new Callback<VM_Comercios>() {
            @Override
            public void onResponse(Call<VM_Comercios> call, Response<VM_Comercios> response) {
                VM_Comercios peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(Tiendas.this,"No se obtuvo",Toast.LENGTH_SHORT).show();
                    return;
                }else{

                    List<Clase_Comercios> get = peticion.Comercios;

                    for (Clase_Comercios mostrar : get){
                        Catalogonombres.add(mostrar.getNombre_tienda());
                    }
                    lvtiendas.setAdapter(arrayAdapter);
                    //----------------------------------------------
                    for (Clase_Comercios mostrartelefonos : get){
                        Catalgotelefonos.add(mostrartelefonos.getTelefono());
                    }

                    ctelefonos = Catalgotelefonos.toArray(new String[Catalgotelefonos.size()]);
                    //-----------------------------------------------
                    for (Clase_Comercios mostrarEstado : get){
                        Catalogoestados.add(mostrarEstado.getEstado());
                    }

                    cestados = Catalogoestados.toArray(new String[Catalogoestados.size()]);
                    //-----------------------------------------------
                    //-----------------------------------------------
                    for (Clase_Comercios mostrarMunicipio : get){
                        Catalogomunicipios.add(mostrarMunicipio.getMunicipio());
                    }

                    cmunicipios = Catalogomunicipios.toArray(new String[Catalogomunicipios.size()]);
                    //-----------------------------------------------
                    //-----------------------------------------------
                    for (Clase_Comercios mostrarColonia : get){
                        Catalogocolonia.add(mostrarColonia.getColonia());
                    }

                    ccolonias = Catalogocolonia.toArray(new String[Catalogocolonia.size()]);
                    //-----------------------------------------------

                    //--------------------------------OBTENER TOKEN
                    for (Clase_Comercios mostrarToken : get){
                        Catalogotokent.add(mostrarToken.getToken());
                    }

                    ctokenes = Catalogotokent.toArray(new String[Catalogotokent.size()]);
                    //-----------------------------------------------
                    //--------------------------------OBTENER ID
                    for (Clase_Comercios mostrarId : get){
                        CatalogoId.add(mostrarId.getId());
                    }

                    cid = CatalogoId.toArray(new String[CatalogoId.size()]);
                    //-----------------------------------------------



                }



            }
            @Override
            public void onFailure(Call<VM_Comercios> call, Throwable t) {
                Toast.makeText(Tiendas.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });




    }//oncreate



    // madar token y mostrar productos
    public void solictarproductos(View view){

        Intent go = new Intent(Tiendas.this,Productos.class);
        go.putExtra("idtienda",id_tienda);
        go.putExtra("tokencomercio",Token_tineda);
        startActivity(go);

    }//

}//clase