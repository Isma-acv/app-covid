package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.Clase_Comercios;
import com.example.appeco_vid.VIEWMODELS.Clase_Productos;
import com.example.appeco_vid.VIEWMODELS.VM_Comercios;
import com.example.appeco_vid.VIEWMODELS.VM_Productos;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Productos extends AppCompatActivity {

    TextView etidtienda, ettoketienda;
    public String idtienda, tokentienda;
    public ListView lvproductos;
    ArrayList<String> CatalogoProductos = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);
        //obtengo los elemntos
        etidtienda = (TextView) findViewById(R.id.textViewidtienda);
        ettoketienda = (TextView) findViewById(R.id.textViewtokentienda);
        idtienda = getIntent().getStringExtra("idtienda");
        tokentienda  = getIntent().getStringExtra("tokencomercio");
        etidtienda.setText(idtienda);
        ettoketienda.setText(tokentienda);
        //elemto de mi list view
        lvproductos = (ListView) findViewById(R.id.lvproductos);
        //--------------------------------------------- Codigo ListView
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.list_item_disenolu,CatalogoProductos);

        //------------------------------------------------    Codigo Peticion   --------------------------------------------------
        ServicioPeticion service = Api.getApi(Productos.this).create(ServicioPeticion.class);
        Call<VM_Productos> iniciarCall = service.get_Productos(idtienda);
        iniciarCall.enqueue(new Callback<VM_Productos>() {
            @Override
            public void onResponse(Call<VM_Productos> call, Response<VM_Productos> response) {
                        VM_Productos peticion = response.body();

                        if (response.body() == null){
                            Toast.makeText(Productos.this,"No se obtuvo",Toast.LENGTH_SHORT).show();
                            return;
                        }else {

                            List<Clase_Productos> get = peticion.productos;

                            for (Clase_Productos mostrar : get) {
                                CatalogoProductos.add("Producto: "+mostrar.getNombre_producto());
                                CatalogoProductos.add("Precio: "+mostrar.getPrecio());
                                CatalogoProductos.add("Contenido: "+mostrar.getContenido_neto());
                            }
                            lvproductos.setAdapter(arrayAdapter);
                            //----------------------------------------------
                        }
            }

            @Override
            public void onFailure(Call<VM_Productos> call, Throwable t) {
                Toast.makeText(Productos.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });


    }//oncreate

    public void GoSolictarproductos(View view){
        Intent go = new Intent(Productos.this,SolicitarProductos.class);
        go.putExtra("tokentineda",tokentienda);
        startActivity(go);
    }
}//Clase