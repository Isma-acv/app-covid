package com.example.appeco_vid.VIEWMODELS;

public class VM_CompletarTienda {

    public String id;
    public String usuario;

    public VM_CompletarTienda(String id, String usuario, String token, String telefono, String estado, String municipio, String colonia, String giro, String cp, String nombretienda) {
        this.id = id;
        this.usuario = usuario;
        this.token = token;
        this.telefono = telefono;
        this.estado = estado;
        this.municipio = municipio;
        this.colonia = colonia;
        this.giro = giro;
        this.cp = cp;
        this.nombretienda = nombretienda;
    }

    public String token;
    public String telefono;
    public String estado;
    public String municipio;
    public String colonia;
    public String giro;
    public String cp;
    public String nombretienda;
    public String estadoe;
    public String detalle;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getGiro() {
        return giro;
    }

    public void setGiro(String giro) {
        this.giro = giro;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getNombretienda() {
        return nombretienda;
    }

    public void setNombretienda(String nombretienda) {
        this.nombretienda = nombretienda;
    }

    public String getEstadoe() {
        return estadoe;
    }

    public void setEstadoe(String estadoe) {
        this.estadoe = estadoe;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

}