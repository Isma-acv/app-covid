package com.example.appeco_vid.VIEWMODELS;

import java.util.ArrayList;

public class VM_Productos {

    public String id;
    public ArrayList<Clase_Productos> productos;

    public VM_Productos(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Clase_Productos> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Clase_Productos> productos) {
        this.productos = productos;
    }


}
