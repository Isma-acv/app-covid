package com.example.appeco_vid;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

public class Fcm extends FirebaseMessagingService {
   String APITOKEN;
    @Override
    public void onNewToken( String s) {
        super.onNewToken(s);
        APITOKEN = s;
        guardarPreferencias();
        Log.e("token", "mi token es: "+s);
        guardarTokenNuevo(s);
    }

    public void guardarTokenNuevo(String s){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("token");
        ref.child("elizabeth_mon").setValue(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String from = remoteMessage.getFrom();

        if(remoteMessage.getData().size() > 0){
            Log.e("TAG","Mi titulo es "+remoteMessage.getData().get("titulo"));
            Log.e("TAG","Mi detalle es "+remoteMessage.getData().get("detalle"));
            Log.e("TAG","Folio venta "+remoteMessage.getData().get("folio"));

            String  titulo = remoteMessage.getData().get("titulo");
            String  detalle = remoteMessage.getData().get("detalle");
            String folio = remoteMessage.getData().get("folio");
            mayorQueOreo(titulo,detalle,folio);
        }

    }// resivir msj

    private void mayorQueOreo(String titulo, String detalle, String folio){

        String id= "mensaje";
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,id);
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){

            NotificationChannel nc = new NotificationChannel(id, "nuevo", NotificationManager.IMPORTANCE_HIGH);
            nc.setShowBadge(true);
            assert nm != null;
            nm.createNotificationChannel(nc);
        }

        builder.setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(titulo)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(detalle)
                .setContentIntent(clicknoti(titulo,detalle, folio))
                .setContentInfo(folio);

        Random random = new Random();
        int idNotify = random.nextInt(8000);

        assert nm != null;
        nm.notify(idNotify,builder.build());

    }//mayor que oreo

    public PendingIntent clicknoti(String titulo, String detalle, String folio){
        Intent it = new Intent(getApplicationContext(), ConfirmarPedido.class);
        it.putExtra("titulo", titulo);
        it.putExtra("detalle", detalle);
        it.putExtra("folio", folio);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(this,0,it,0);

    }

    //Guardar token cuando se instale
    public void guardarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }

}//clase
