package com.example.appeco_vid.VIEWMODELS;

public class Clase_Productos {
    public String nombre_producto;
    public String precio;
    public String contenido_neto;

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getContenido_neto() {
        return contenido_neto;
    }

    public void setContenido_neto(String contenido_neto) {
        this.contenido_neto = contenido_neto;
    }
}
