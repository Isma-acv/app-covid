package com.example.appeco_vid;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appeco_vid.VIEWMODELS.ComerciosVo;

import java.util.ArrayList;

public class AdaptadorComercios extends RecyclerView.Adapter<AdaptadorComercios.ViewHolderComercios> implements View.OnClickListener{

    ArrayList<ComerciosVo> listacomercios;
    private View.OnClickListener listener;

    public AdaptadorComercios(ArrayList<ComerciosVo> listacomercios) {
        this.listacomercios = listacomercios;
    }

    //agrefo onformacon a mi item
    @NonNull
    @Override
    public ViewHolderComercios onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_comercios, null, false);

        view.setOnClickListener(this);

        return new ViewHolderComercios(view);

    }

    //aqui se llenan los datos
    @Override
    public void onBindViewHolder(@NonNull ViewHolderComercios holder, int position) {
        holder.EtiNombre.setText(listacomercios.get(position).getNombre());
        holder.EtiInformacion.setText(listacomercios.get(position).getInfo());
        holder.foto.setImageResource(listacomercios.get(position).getFoto());
    }

    //tamaño de la lista
    @Override
    public int getItemCount() {
        return listacomercios.size();
    }

    // creo metodo de mi button
    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;

    }

    // metodo onclick del button
    @Override
    public void onClick(View view) {
        if (listener!= null){
            listener.onClick(view);
        }
    }

    //hago referencia a mis comercios y elemtos
    public class ViewHolderComercios extends RecyclerView.ViewHolder {

        TextView EtiNombre,EtiInformacion;
        ImageView foto;
        public ViewHolderComercios(@NonNull View itemView) {
            super(itemView);

            EtiNombre = (TextView) itemView.findViewById(R.id.idNombre);
            EtiInformacion = (TextView) itemView.findViewById(R.id.idInfo);
            foto = (ImageView) itemView.findViewById(R.id.idImagen);

        }
    }


}//clase
