package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.VM_Registro;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {
    String seleccion, APITOKEN;
    private Spinner spinnercuenta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        // mostra el token original
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
            APITOKEN = token;
        }

        spinnercuenta = (Spinner) findViewById(R.id.spinnercuenta);
        String [] opciones = {"cliente","comerciante"};
        ArrayAdapter <String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, opciones);
        spinnercuenta.setAdapter(adapter);

    }//Oncrete


    public void Goiniciar(View view){
        Intent Irnoti = new Intent(Registro.this,MainActivity.class);
        startActivity(Irnoti);
    }//ir a crear notificacion

    //metodo para crear cuenta
    public void CrearCuenta(View view){
        //Obtengo mis elementos y token
        seleccion = spinnercuenta.getSelectedItem().toString();
        EditText EdtNombre  = (EditText) findViewById(R.id.editTextTextNombre);
        EditText EdtPassword  = (EditText) findViewById(R.id.editTextTextContrasena);
        EditText EdtPasswordC  = (EditText) findViewById(R.id.editTextTextConContrasena);
        EditText EdtMail  = (EditText) findViewById(R.id.editTextTextCorreo);
        EditText EdtUsuario  = (EditText) findViewById(R.id.editTextTextUsuario);
        String nombre = EdtNombre.getText().toString();
        final String password = EdtPassword.getText().toString();
        String passwordC = EdtPasswordC.getText().toString();
        String mail = EdtMail.getText().toString();
        String usuario = EdtUsuario.getText().toString();
        String cuenta = seleccion;
        String token = APITOKEN;

        //verifico que mis campos no sean nulos

        if(TextUtils.isEmpty(nombre)){
            EdtNombre.setError("Porfavor ingrese su nombre");
            EdtNombre.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(mail)){
            EdtMail.setError("Porfavor ingrese su correo");
            EdtMail.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(usuario)){
            EdtUsuario.setError("Porfavor ingrese un nombre de suario");
            EdtUsuario.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(password)){
            EdtPassword.setError("Porfavor escriba una contraseña");
            EdtPassword.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(passwordC)){
            EdtPasswordC.setError("Porfavor repita su contraseña");
            EdtPasswordC.requestFocus();
            return;
        }
        if(!password.equals(passwordC)){
            EdtPassword.setError("LAS CONTRASEÑAS NO COINCIDEN");
            EdtPassword.requestFocus();
            EdtPasswordC.setError("LAS CONTRASEÑAS NO COINCIDEN");
            EdtPasswordC.requestFocus();
            return;
        }

        //Realizo peticion de registro
        ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
        Call<VM_Registro> iniciarCall = service.get_Registro(usuario,password,mail,cuenta,token,nombre);
        iniciarCall.enqueue(new Callback<VM_Registro>() {
            @Override
            public void onResponse(Call<VM_Registro> call, Response<VM_Registro> response) {
                VM_Registro peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(Registro.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (peticion.estado == "true"){
                    Toast.makeText(Registro.this,"Ya puedes iniciar",Toast.LENGTH_LONG).show();
                    Intent go = new Intent(Registro.this,MainActivity.class);
                    startActivity(go);


                }else {
                    Toast.makeText(Registro.this,""+peticion.detalle,Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<VM_Registro> call, Throwable t) {
                Toast.makeText(Registro.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });



    }// Crear cuenta


}//Clase