package com.example.appeco_vid.SERVICIO;

import com.example.appeco_vid.VIEWMODELS.VM_ActivarCuenta;
import com.example.appeco_vid.VIEWMODELS.VM_AddProducto;
import com.example.appeco_vid.VIEWMODELS.VM_Comercios;
import com.example.appeco_vid.VIEWMODELS.VM_CompletarCliente;
import com.example.appeco_vid.VIEWMODELS.VM_CompletarTienda;
import com.example.appeco_vid.VIEWMODELS.VM_Confirmar;
import com.example.appeco_vid.VIEWMODELS.VM_Crearventa;
import com.example.appeco_vid.VIEWMODELS.VM_Datoscliente;
import com.example.appeco_vid.VIEWMODELS.VM_Logeo;
import com.example.appeco_vid.VIEWMODELS.VM_Productos;
import com.example.appeco_vid.VIEWMODELS.VM_Registro;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.PATCH;
import retrofit2.http.POST;

public interface ServicioPeticion {
    //inicira sesion
    @FormUrlEncoded
    @POST("index.php")
    Call<VM_Logeo> get_Logeo(@Field("username")String usuario, @Field("password")String password);

    //crear ususario
    @FormUrlEncoded
    @POST("apiregistro.php")
    Call<VM_Registro> get_Registro(@Field("usuario")String usuario, @Field("password")String password, @Field("mail")String mail, @Field("cuenta")String cuenta, @Field("token")String token, @Field("nombre")String nombre);

    //activar cuenta
    @FormUrlEncoded
    @POST("apiactivar.php")
    Call<VM_ActivarCuenta> get_Activar(@Field("id")String id, @Field("code")String code);

    //buscar tiendas
    @FormUrlEncoded
    @POST("apibuscarcomercio.php")
    Call<VM_Comercios> get_Comercios(@Field("giro")String giro);

    //buscar productos
    @FormUrlEncoded
    @POST("apiconsultarproductos.php")
    Call<VM_Productos> get_Productos(@Field("id")String id);

    //buscar datoscliente
    @FormUrlEncoded
    @POST("apidatoscliente.php")
    Call<VM_Datoscliente> get_Datoscliente(@Field("id")String id);

    //Crear venta
    @FormUrlEncoded
    @POST("apicrearventa.php")
    Call<VM_Crearventa> get_Venta(@Field("titulo")String titulo, @Field("contenido")String contenido, @Field("tokencliente") String tokencliente, @Field("idcliente")String idcliente, @Field("estado")String estado, @Field("municipio")String municipio, @Field("colonia")String colonia, @Field("calle")String calle, @Field("cp")String cp, @Field("folio")String folio);

    //Completar perfil cliente
    @FormUrlEncoded
    @POST("apicompletarcliente.php")
    Call<VM_CompletarCliente> get_CCliente(@Field("id")String id, @Field("usuario")String usuario, @Field("token")String token, @Field("telefono")String telefono, @Field("estado")String estado, @Field("municipio")String municipio, @Field("colonia")String colonia, @Field("cp")String cp, @Field("calle")String calle, @Field("calleruno")String calleuno, @Field("callerdos")String calledos, @Field("referencia")String referencia);

    //buscar agregarProducto
    @FormUrlEncoded
    @POST("apiagregarpro.php")
    Call<VM_AddProducto> get_Addproductos(
            @Field("id")String id,
            @Field("usuario")String usuario,
            @Field("token")String token,
            @Field("nomproducto")String nomproducto,
            @Field("precio")String precio,
            @Field("contenido")String contenido);


    //compltera comerico
    @FormUrlEncoded
    @POST("apicompletarcomercio.php")
    Call<VM_CompletarTienda> get_tienda(
                    @Field("id")String id,
                    @Field("usuario")String usuario,
                    @Field("nombretienda")String nombretienda,
                    @Field("giro")String giro,
                    @Field("telefono")String telefono,
                    @Field("correo")String correo,
                    @Field("estado")String estado,
                    @Field("municipio")String municipio,
                    @Field("colonia")String colonia,
                    @Field("cp")String cp,
                    @Field("token")String token);

    @FormUrlEncoded
    @POST("apiconfirmarventa.php")
    Call<VM_Confirmar> get_Confirmar(
            @Field("folio")String folio,
            @Field("costop")String costo,
            @Field("tiempo")String tiempo,
            @Field("tokencomercio")String tokencomercio,
            @Field("idcomercio")String idcomercio);


}
