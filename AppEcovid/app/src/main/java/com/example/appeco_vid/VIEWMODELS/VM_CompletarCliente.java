package com.example.appeco_vid.VIEWMODELS;

public class VM_CompletarCliente {
    public String id;
    public String usuario;
    public String token;
    public String telefono;
    public String estado;
    public String municipio;
    public String colonia;
    public String calle;
    public String cp;
    public String referencia;

    public VM_CompletarCliente(String id, String usuario, String token, String telefono, String estado, String municipio, String colonia, String calle, String cp, String calleuno, String calledos, String referencia) {
        this.id = id;
        this.usuario = usuario;
        this.token = token;
        this.telefono = telefono;
        this.estado = estado;
        this.municipio = municipio;
        this.colonia = colonia;
        this.calle = calle;
        this.cp = cp;
        this.calleuno = calleuno;
        this.calledos = calledos;
        this.referencia = referencia;
    }

    public String calleuno;
    public String calledos;
    public String estadoe;
    public String detalle;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getCalleuno() {
        return calleuno;
    }

    public void setCalleuno(String calleuno) {
        this.calleuno = calleuno;
    }

    public String getCalledos() {
        return calledos;
    }

    public void setCalledos(String calledos) {
        this.calledos = calledos;
    }

    public String getEstadoe() {
        return estadoe;
    }

    public void setEstadoe(String estadoe) {
        this.estadoe = estadoe;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }


}
