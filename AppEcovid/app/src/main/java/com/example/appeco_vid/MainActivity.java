package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.VM_Logeo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public String  NAMEUSUARIO, IDUSUARIO, CUENTA;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //obtener elementos

        TextView ttoken = (TextView) findViewById(R.id.textViewtoken);
        // mostra el token original
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
          //  ttoken.setText(token);
        }else {
           // ttoken.setText("no se octuvo");
        }

    }//oncreate



public void IniciarSesion(View view){
        // obtengo mis elementos
    EditText EdtUsuario = (EditText) findViewById(R.id.editTextTextUsuarioM);
    EditText EdtPassword = (EditText) findViewById(R.id.editTextTextPasswordM);
    String usuario = EdtUsuario.getText().toString();
    String password = EdtPassword.getText().toString();
    //verifico que no sean nulos

    if(TextUtils.isEmpty(usuario)){
        EdtUsuario.setError("Porfavor Ingrese su Usuario");
        EdtUsuario.requestFocus();
        return;
    }
    if(TextUtils.isEmpty(password)){
        EdtPassword.setError("Ups! se le olvida lo mas Importante");
        EdtPassword.requestFocus();
        return;
    }

    //--incio peticon de logeo
    ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
    Call<VM_Logeo> iniciarCall = service.get_Logeo(EdtUsuario.getText().toString(), EdtPassword.getText().toString());
    iniciarCall.enqueue(new Callback<VM_Logeo>() {
        @Override
        public void onResponse(Call<VM_Logeo> call, Response<VM_Logeo> response) {

            VM_Logeo peticion = response.body();

            if (response.body() == null){
                Toast.makeText(MainActivity.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                return;
            }
            if (peticion.estado == "true"){
                NAMEUSUARIO = peticion.username;
                guardarPreferenciasusuario();
                IDUSUARIO = peticion.id ;
                guardarPreferenciasusuarioid();
                CUENTA = peticion.cuenta;
                guardarPreferenciascuenta();
                if (peticion.status.equals("0")){
                    Toast.makeText(MainActivity.this,"Bienvenido: "+NAMEUSUARIO,Toast.LENGTH_LONG).show();
                    Intent go = new Intent(MainActivity.this,ActivarCuenta.class);
                    startActivity(go);
                }else {

                    if (peticion.cuenta.equals("cliente")){
                        Intent go = new Intent(MainActivity.this,Panel.class);
                        startActivity(go);
                    }else{
                        Intent gio = new Intent(MainActivity.this,PanelComerciante.class);
                        startActivity(gio);
                    }
                }


            }else {
                Toast.makeText(MainActivity.this,"El usuario no existe",Toast.LENGTH_LONG).show();
            }

        }

        @Override
        public void onFailure(Call<VM_Logeo> call, Throwable t) {
            Toast.makeText(MainActivity.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
        }
    });


}//IniciarSesion



    // guardar nombre de usuario
    public void guardarPreferenciasusuario(){
        SharedPreferences preferenciasusuario = getSharedPreferences("credencialesusuario", Context.MODE_PRIVATE);
        String username = NAMEUSUARIO;
        SharedPreferences.Editor editor = preferenciasusuario.edit();
        editor.putString("USUARIO", username);
        editor.commit();
    }
    //-

    // guardar id de usuario
    public void guardarPreferenciasusuarioid(){
        SharedPreferences preferenciasusuarioid = getSharedPreferences("credencialesusuarioid", Context.MODE_PRIVATE);
        String id = IDUSUARIO;
        SharedPreferences.Editor editor = preferenciasusuarioid.edit();
        editor.putString("ID", id);
        editor.commit();
    }
    //-

    // guardar el tipo de cuenta
    public void guardarPreferenciascuenta(){
        SharedPreferences preferenciascuenta = getSharedPreferences("credencialescuenta", Context.MODE_PRIVATE);
        String cuenta = CUENTA;
        SharedPreferences.Editor editor = preferenciascuenta.edit();
        editor.putString("CUENTA", cuenta);
        editor.commit();
    }
    //-


    public void GoNoti(View view){
        Intent Irnoti = new Intent(MainActivity.this,SolicitarProductos.class);
        startActivity(Irnoti);
    }//ir a crear notificacion
}//clase