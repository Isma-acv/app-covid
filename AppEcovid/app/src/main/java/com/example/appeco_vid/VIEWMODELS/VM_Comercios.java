package com.example.appeco_vid.VIEWMODELS;

import java.util.ArrayList;

public class VM_Comercios {

    public String giro;
    public ArrayList<Clase_Comercios> Comercios;

    public VM_Comercios(String giro) {
        this.giro = giro;
    }


    public String getGiro() {
        return giro;
    }

    public void setGiro(String giro) {
        this.giro = giro;
    }

    public ArrayList<Clase_Comercios> getComercios() {
        return Comercios;
    }

    public void setComercios(ArrayList<Clase_Comercios> comercios) {
        Comercios = comercios;
    }


}
