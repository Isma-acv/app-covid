package com.example.appeco_vid.VIEWMODELS;

public class VM_Confirmar {

    public String folio;

    public VM_Confirmar(String folio, String costop, String tiempo, String tokencomercio, String idcomercio) {
        this.folio = folio;
        this.costop = costop;
        this.tiempo = tiempo;
        this.tokencomercio = tokencomercio;
        this.idcomercio = idcomercio;
    }

    public String costop;
    public String tiempo;
    public String tokencomercio;
    public String idcomercio;
    public String estado;
    public String detalle;

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCostop() {
        return costop;
    }

    public void setCostop(String costop) {
        this.costop = costop;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getTokencomercio() {
        return tokencomercio;
    }

    public void setTokencomercio(String tokencomercio) {
        this.tokencomercio = tokencomercio;
    }

    public String getIdcomercio() {
        return idcomercio;
    }

    public void setIdcomercio(String idcomercio) {
        this.idcomercio = idcomercio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }



}
