package com.example.appeco_vid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.appeco_vid.API.Api;
import com.example.appeco_vid.SERVICIO.ServicioPeticion;
import com.example.appeco_vid.VIEWMODELS.VM_AddProducto;
import com.example.appeco_vid.VIEWMODELS.VM_Logeo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgregarProductos extends AppCompatActivity {

    public String idtienda, usuariotienda, tokentienda,producto,precio,contenido;
    EditText edtprecio, edtproducto,edtcontenido;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_productos);
        //elemntos  que ocupoe
        edtproducto = (EditText) findViewById(R.id.editTextTextProducto1);
        edtprecio = (EditText) findViewById(R.id.editTextTextPrecio1);
        edtcontenido = (EditText) findViewById(R.id.editTextTextContenido1);


        //ver id
        SharedPreferences preferenciasusuarioid = getSharedPreferences("credencialesusuarioid", Context.MODE_PRIVATE);
        String id = preferenciasusuarioid.getString("ID", "");
        if (id != ""){
           idtienda = id;
        }
        //ver nombre de usuario
        SharedPreferences preferenciasusuario = getSharedPreferences("credencialesusuario", Context.MODE_PRIVATE);
        String username = preferenciasusuario.getString("USUARIO", "");
        if (username != ""){
            usuariotienda = username;
        }
        //ver token
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
            tokentienda = token;
        }


    }//oncreate

    public void Addproductos(View view) {
        //petiicon registrar productos

         producto= edtproducto.getText().toString();
        precio = edtprecio.getText().toString();
        contenido = edtcontenido.getText().toString();

        if(TextUtils.isEmpty(producto)){
            edtproducto.setError("Es necesario este campo");
            edtproducto.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(precio)){
            edtprecio.setError("Es necesario este campo");
            edtprecio.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(contenido)){
            edtcontenido.setError("Es necesario este campo");
            edtcontenido.requestFocus();
            return;
        }

        //peticio

        ServicioPeticion service = Api.getApi(AgregarProductos.this).create(ServicioPeticion.class);
         Call<VM_AddProducto> iniciarCall = service.get_Addproductos(idtienda,usuariotienda,tokentienda,producto,precio,contenido);
         iniciarCall.enqueue(new Callback<VM_AddProducto>() {
             @Override
             public void onResponse(Call<VM_AddProducto> call, Response<VM_AddProducto> response) {

                 VM_AddProducto peticion = response.body();

                 if (response.body() == null){
                     Toast.makeText(AgregarProductos.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                     return;
                 }
                 if (peticion.estado == "true"){
                     Toast.makeText(AgregarProductos.this,"Producto publicado: ",Toast.LENGTH_LONG).show();
                     }else {
                         Toast.makeText(AgregarProductos.this,""+peticion.detalle,Toast.LENGTH_LONG).show();
                     }

             }

             @Override
             public void onFailure(Call<VM_AddProducto> call, Throwable t) {
                 Toast.makeText(AgregarProductos.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
             }
         });


    }


}//clsae