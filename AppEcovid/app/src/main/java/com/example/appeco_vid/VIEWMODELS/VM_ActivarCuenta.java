package com.example.appeco_vid.VIEWMODELS;

public class VM_ActivarCuenta {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String id;
    public String code;
    public String estado;
    public String cuenta;

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public VM_ActivarCuenta(String id, String code){
        this.id = id;
        this.code = code;
    }

}
